//
//  LicenseModel.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/22.
//

import Foundation

struct License: Codable {
    var key: String
    var name: String?
    var url: String?
    
    private enum CodingKeys: String, CodingKey {
        case key
        case name
        case url
    }
}
