//
//  RepositoryModel.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/22.
//

import Foundation

struct Repository: Codable {
    let id :Int
    let name: String?
    let node_id: String?
    let full_name: String?
    let description: String?
    let owner: Owner?
    let license: License?
    var score: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case node_id
        case full_name
        case description
        case owner
        case license
        case score
    }
}

