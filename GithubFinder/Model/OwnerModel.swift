//
//  OwnerModel.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/22.
//

import Foundation

struct Owner: Codable, Identifiable {
    var id: Int
    var login: String?
    var avatar_url: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case login
        case avatar_url
    }
}
