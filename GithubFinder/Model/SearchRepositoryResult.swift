//
//  SearchRepositoryResult.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/23.
//

import Foundation

struct SearchRepositoryResult: Codable {
    let total_count: Int
    let incomplete_results: Bool
    let items : [Repository]?
    
    private enum CodingKeys: String, CodingKey{
        case total_count
        case incomplete_results
        case items
    }
    
}
