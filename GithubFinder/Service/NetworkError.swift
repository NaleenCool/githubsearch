//
//  NetworkError.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/22.
//

import Foundation

public enum NetworkError: String, Error {
    
    case encodingFailed = "Parameter encoding failed."
    
    case decodingFailed = "Parameter decoding failed."
    
    case missingURL = "URL is nil."
    
    case connectionFailed = "Internet connection is failed"
    
    case invalidRequest = "Invalid request"
    
    case invalidResponse = "Invalid response"
    
    case dataLoadingError = "There is a problem of loading data"
    
    case jsonDecodingError = "There is a problem of data decoding"
}
