//
//  SearchEndpoint.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/21.
//

import Foundation

struct SearchEndpoint {
    var queryItems: [URLQueryItem] = []
}

extension SearchEndpoint {
    var url: URL {
        var components = URLComponents()
        components.scheme = Constant.SCHEMA
        components.host = Constant.HOST
        components.path = Constant.PATH
        components.queryItems = queryItems
        
        guard let url = components.url else {
            preconditionFailure("Invalid URL components: \(components)")
        }
        return url
    }
}

extension SearchEndpoint {
    static func searchRepositories(query: String) -> Self {
        return SearchEndpoint(
            queryItems: [
                URLQueryItem(name: "q",
                             value: "\(query)")
            ]
        )
    }
}
