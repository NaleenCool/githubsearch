//
//  ConnectivityMonitor.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/21.
//

import Foundation
import Network

struct ConnectivityMonitor {
    static let shared = ConnectivityMonitor()
    private let monitor = NWPathMonitor()
    func setUp() {
        monitor.pathUpdateHandler = { _ in
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
    func isOnline() -> Bool {
        return monitor.currentPath.status == .satisfied
    }
}
