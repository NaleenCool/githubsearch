//
//  NetworkService.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/21.
//

import Foundation

// MARK: Delegate protocol
protocol NetworkServiceDelegate: AnyObject{
    func didFinishURLRequest(withRepos data:SearchRepositoryResult)
}

class NetworkService{
    
    // MARK: Delegate
    weak var delegate:NetworkServiceDelegate?
    
    func fetchRepositories(withSearchKey key:String){
        
        let searchURL = SearchEndpoint.searchRepositories(query: key)
        
        guard ConnectivityMonitor.shared.isOnline() else {
            print("No connectivity")
            return
        }
        
        // Create a dataTask to get the github repositories
        URLSession.shared.dataTask(with: searchURL.url, completionHandler: {(data, response, error) in
            
            // Check if data isn't nil
            guard let data = data else{
                print("Data is nil")
                return
            }
            
            do{
                // Set up the json decoder
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = .iso8601
                
                // Try to decode json from the aquired data
                let searchResult = try jsonDecoder.decode(SearchRepositoryResult.self, from: data)
                print("Public Feeds :",searchResult)
                // Take the delegate task to the main queue
                
                DispatchQueue.main.async {
                    self.delegate?.didFinishURLRequest(withRepos: searchResult)
                }
                
            }catch let jsonError{
                print("error")
                print(jsonError.localizedDescription)
            }
            
        }).resume()
    }
}

