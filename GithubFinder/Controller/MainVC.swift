//
//  MainVC.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/21.
//

import UIKit
import Combine

class MainVC: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var reposList = [Repository]()
    var dafaultSearchKey = "a"
    
    private var repositoryService = NetworkService(){
        didSet{
            repositoryService.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCells()
        setupNavigationBar()
        repositoryService = NetworkService()
        //get initial data
    }
    
    override func viewWillAppear(_ animated: Bool) {
        repositoryService.fetchRepositories(withSearchKey: dafaultSearchKey)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    private func setupNavigationBar(){
        self.navigationItem.title = Constant.MAIN_NAVIGATION_TITLE
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.blue]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    private func registerTableViewCells() {
        let githubViewCell = UINib(nibName: Constant.GITHUB_VIEW_CELL,
                                  bundle: nil)
        self.tableView.register(githubViewCell,
                                forCellReuseIdentifier: Constant.GITHUB_VIEW_CELL)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constant.GITHUB_VIEW_CELL) as? GithubViewCell{
            cell.userImageView.image = UIImage(named: "user")
            if let repoName = reposList[indexPath.row].name {
                cell.repoName.text = repoName
            }
            if let repoDescription = reposList[indexPath.row].description {
                cell.repoDescription.text = repoDescription
            }

            return cell
        }
        
        return GithubViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reposList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

// MARK: SearchBarDelegate
// Handle the searchBar events
extension MainVC: UISearchBarDelegate{
    // Hide the keyboard and cancel button
    override func resignFirstResponder() -> Bool {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        return super.resignFirstResponder()
    }
    
    // React to user pressing the cancel button
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    // React to user starting to edit textfield
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        guard !searchText.isEmpty else {return}
        dafaultSearchKey = searchText
        repositoryService.fetchRepositories(withSearchKey: searchText)
    }
    
    // Search for repos
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
}

// MARK: FlickrFeedURLHelperDelegate
extension MainVC: NetworkServiceDelegate {
    // Sort and set the flickr posts after fetching them from public feed
    func didFinishURLRequest(withRepos data:SearchRepositoryResult) {
        print("data:",data)
        if let repos = data.items {
            reposList = repos
            tableView.reloadData()
        }
    }
    
}
