//
//  Constant.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/21.
//

import Foundation

struct Constant {
    
    //Networking
    static let SCHEMA = "https"
    static let HOST = "api.github.com"
    static let PATH = "/search/repositories"
    
    //Navigation bar titles
    static let MAIN_NAVIGATION_TITLE = "GITHUB SEARCH"
    
    //Table view cell identifiers
    static let GITHUB_VIEW_CELL = "GithubViewCell"
}
