//
//  AppDelegate.swift
//  GithubFinder
//
//  Created by Naleen Dissanayake on 2022/03/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        ConnectivityMonitor.shared.setUp()
        return true
    }
}

